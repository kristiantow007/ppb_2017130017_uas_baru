package com.example.chatapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    TextInputEditText Etemail, EtPassword;
    TextView Login;
    Button btnRegistrasi, btnLogin;
    ProgressDialog progressDialog;

    FirebaseAuth mAuth;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        Etemail       = findViewById(R.id.txtEmail);
        EtPassword    = findViewById(R.id.txtPassword);
        btnRegistrasi = findViewById(R.id.Register);
        Login         = findViewById(R.id.txtLogin);

        mAuth = FirebaseAuth.getInstance();

        btnLogin    = findViewById(R.id.BtnLogin);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Registering User...");

        // Set Handle Register Click
        btnRegistrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = Etemail.getText().toString().trim();
                String password = EtPassword.getText().toString().trim();

                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    Etemail.setError("Invalid Email");
                    Etemail.setFocusable(true);
                } else if (password.length() < 6) {
                    EtPassword.setError("Password length at least 6 characters");
                    EtPassword.setFocusable(true);
                } else {
                    registerUser(email, password, v);
                }

            }

        });

        // Set Handle Login Click
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    // Method Register
    private void registerUser (String txtEmail, String txtPassword, View v){
        progressDialog.show();
        mAuth.createUserWithEmailAndPassword(txtEmail, txtPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success dismiss dialog and start register activity
                            progressDialog.dismiss();
                            FirebaseUser user = mAuth.getCurrentUser();

                            // Get user email and uid from auth
                            String email = user.getEmail();
                            String uid = user.getUid();

                            //When user is registered store user info to firebase realtime database too
                            //Hashmap
                            HashMap<Object, String> hashMap = new HashMap<>();
                            //Putinfo
                            hashMap.put("email", email);
                            hashMap.put("uid", uid);
                            hashMap.put("name", ""); // Add Later Edit Profile
                            hashMap.put("onlineStatus", "online"); // Add Later Edit Profile
                            hashMap.put("phone", ""); // Add Later Edit Profile
                            hashMap.put("image", ""); // Add Later Edit Profile
                            hashMap.put("cover", ""); // Add Later Edit Profile

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            // Path to store user
                            DatabaseReference reference = database.getReference("Users");
                            // Put data within hashman
                            reference.child(uid).setValue(hashMap);

                            // Snackbar Pengganti Toast
                            Snackbar snackbar = Snackbar.make(v, "Registrasi Berhasil", Snackbar.LENGTH_LONG);
                            snackbar.show();

                            startActivity(new Intent(RegisterActivity.this, DashboardActivity.class));
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            progressDialog.dismiss();
                            // Snackbar Pengganti Toast
                            Snackbar snackbar = Snackbar.make(v, "Authentication Failed", Snackbar.LENGTH_LONG);
                            snackbar.show();
                           //  Toast.makeText(RegisterActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                // Snackbar Pengganti Toast
                Snackbar snackbar = Snackbar.make(v, ""+e.getMessage(), Snackbar.LENGTH_LONG);
                snackbar.show();

               //  Toast.makeText(RegisterActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}