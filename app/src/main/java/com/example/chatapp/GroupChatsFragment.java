package com.example.chatapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.example.chatapp.Adapter.AdapterGroupChatsList;
import com.example.chatapp.Model.ModelGroupChatsList;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GroupChatsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GroupChatsFragment extends Fragment {

    private RecyclerView groupsRv;
    private FirebaseAuth firebaseAuth;
    private ArrayList<ModelGroupChatsList> groupChatsLists;
    private AdapterGroupChatsList adapterGroupChatsList;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public GroupChatsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GroupChatsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GroupChatsFragment newInstance(String param1, String param2) {
        GroupChatsFragment fragment = new GroupChatsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group_chats, container, false);

        groupsRv = view.findViewById(R.id.groupsRv);

        firebaseAuth = FirebaseAuth.getInstance();

        loadGroupChatsList();

        return  view;
    }

    private void loadGroupChatsList() {
        groupChatsLists = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Groups");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // If current user's uid exists in participants list of group then show that group
                groupChatsLists.clear();
                for(DataSnapshot ds: snapshot.getChildren()){
                    if(!ds.child("Participants").child(firebaseAuth.getUid()).exists()){
                        ModelGroupChatsList model = ds.getValue(ModelGroupChatsList.class);
                        groupChatsLists.add(model);
                    }
                }
                adapterGroupChatsList = new AdapterGroupChatsList(getActivity(),groupChatsLists);
                groupsRv.setAdapter(adapterGroupChatsList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void SearchGroupChatsList(final String query) {
        groupChatsLists = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Groups");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // If current user's uid exists in participants list of group then show that group
                groupChatsLists.clear();
                for(DataSnapshot ds: snapshot.getChildren()){
                    if(!ds.child("Participants").child(firebaseAuth.getUid()).exists()){

                        // Search by Group title
                        if(ds.child("groupTitle").toString().toLowerCase().contains(query.toLowerCase())){
                            ModelGroupChatsList model = ds.getValue(ModelGroupChatsList.class);
                            groupChatsLists.add(model);
                        }
                    }
                }
                adapterGroupChatsList = new AdapterGroupChatsList(getActivity(),groupChatsLists);
                groupsRv.setAdapter(adapterGroupChatsList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    private void checkUserStatus(){
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if(user !=null){

        }else{
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        }

    }

    // Panggil item Menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main,menu);
        // Search view
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        // Search Listener // Panggil method cari teman
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String q) {
                // called when use button from keyboard / key pressed
                // if search query is not empty then search
                if(!TextUtils.isEmpty(q.trim())){
                    // panggil methode cari teman
                    SearchGroupChatsList(q);

                } else {
                    // jika textbox kosong teman tidak dipanggil
                    loadGroupChatsList();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String q) {
                // called when use button from keyboard / key pressed
                // if search query is not empty then search
                if(!TextUtils.isEmpty(q.trim())){
                    // search text contains text, search it
                    SearchGroupChatsList(q);

                } else {
                    // Search text empty, get all users
                    loadGroupChatsList();
                }
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    // Show menu
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true); // to show menu item in fragment

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    // Menu logout
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_logout){
            firebaseAuth.signOut();
            checkUserStatus();
        } // Go to group activity
        else if ( id==R.id.action_create_group) {
            startActivity(new Intent(getActivity(), CreateGroupActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }


}
