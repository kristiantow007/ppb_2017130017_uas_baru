package com.example.chatapp.Model;

public class ModelChatlist {

    String id; // We will need this ID to get chat list, sender/receiver uid

    public ModelChatlist() {
    }

    public ModelChatlist(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
