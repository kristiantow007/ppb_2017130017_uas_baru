package com.example.chatapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.android.material.button.MaterialButton;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // Set Menu Get Started, ketika aplikasi dibuka
    // --------------------------------------------- //
    // Panggil Import'an
    private OnboardingAdapter onboardingAdapter;
    private LinearLayout LayoutOnboardingIndicators;
    private MaterialButton buttonOnboardingAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LayoutOnboardingIndicators  = (LinearLayout) findViewById(R.id.layoutOnboardingIndicator);
        buttonOnboardingAction      = (MaterialButton) findViewById(R.id.buttonOnboardingAction);

    // Panggil Method
        setupOnboardingItems();
        setupOnboardingIndicators();
        setCurrentOnboardingIndicators(0);
        setupButton();

    }


    // Set Data Kedalam Array
    private void setupOnboardingItems(){

        // Masukkan Data Kedalam List
        List<OnboardingItem> onboardingItems = new ArrayList<>();

        // Set Tittle, Deskripsi dan Gambar untuk Onboarding Page 1
        OnboardingItem itemChat      = new OnboardingItem();
        itemChat.setTitle("Welcome");
        itemChat.setDescription("Selamat Datang di Aplikasi Chat !!");
        itemChat.setImage(R.drawable.robot);

        // Set Tittle, Deskripsi dan Gambar untuk Onboarding Page 2
        OnboardingItem itemEmail   = new OnboardingItem();
        itemEmail.setTitle("Email");
        itemEmail.setDescription("Pendaftaran Mudah Hanya Dengan Email, Ayo Segera Daftarkan Dirimu !");
        itemEmail.setImage(R.drawable.email);

        // Set Tittle, Deskripsi dan Gambar untuk Onboarding Page 3
        OnboardingItem itemContact = new OnboardingItem();
        itemContact.setTitle("Chat");
        itemContact.setDescription("Ngobrol Kapan Saja Jadi Lebih Mudah Dengan Fitur Real Time, Ayo Segera Dicoba !");
        itemContact.setImage(R.drawable.talk);

        onboardingItems.add(itemChat);
        onboardingItems.add(itemEmail);
        onboardingItems.add(itemContact);



        onboardingAdapter = new OnboardingAdapter(onboardingItems);

    }

    // Set Design untuk Onboarding
    private void setupOnboardingIndicators(){

        // Set Posisi dan Design untuk Indikator

        ImageView[] indicators = new ImageView[onboardingAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams  = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(8,0,8,0);
        for(int i = 0; i < indicators.length; i ++){
            indicators[i]   = new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(
                    getApplicationContext(),
                    R.drawable.indicator_inactive
            ));
            indicators[i].setLayoutParams(layoutParams);
            LayoutOnboardingIndicators.addView(indicators[i]);
        }
    }

    // Set Posisi untuk Onboarding
    @SuppressLint("SetTextI18n")
    private void setCurrentOnboardingIndicators(int index){

        // Set Design Indikator untuk status Start dan Next
        // Jika sudah sampai pada max page design berganti
        int childCount  = LayoutOnboardingIndicators.getChildCount();
        for (int i = 0; i <childCount; i++){
            ImageView imageView = (ImageView) LayoutOnboardingIndicators.getChildAt(i);
            if (i == index) {
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(),
                                R.drawable.indicator_active
                        ));
            }
            else {
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(),
                                R.drawable.indicator_inactive
                        ));
            }
        }

        // Max Page = 3
        // Jika masih dalam page 1 dan 2 maka text = "Start"
        // Jika sudah dalam page 3 maka text = "Next"
        if (index == onboardingAdapter.getItemCount() -1){
            buttonOnboardingAction.setText("Start");
        }
        else {
            buttonOnboardingAction.setText("Next");
        }

    }

    // Handle Button ketika menggeser layar
    private void setupButton(){

        final ViewPager2 onboardingViewPager  = findViewById(R.id.onboardingViewPager);
        onboardingViewPager.setAdapter(onboardingAdapter);
        onboardingViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setCurrentOnboardingIndicators(position);
            }
        });

        buttonOnboardingAction.setOnClickListener(v -> {
            if(onboardingViewPager.getCurrentItem() +1 < onboardingAdapter.getItemCount()){
                onboardingViewPager.setCurrentItem(onboardingViewPager.getCurrentItem()+1);
            }
            else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });
    }
}