package com.example.chatapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.GroupChatActivity;
import com.example.chatapp.Model.ModelGroupChatsList;
import com.example.chatapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class AdapterGroupChatsList extends RecyclerView.Adapter<AdapterGroupChatsList.HolderGroupChatList>{

    private Context context;
    private ArrayList<ModelGroupChatsList> groupChatsLists;

    public AdapterGroupChatsList(Context context, ArrayList<ModelGroupChatsList> groupChatsLists) {
        this.context = context;
        this.groupChatsLists = groupChatsLists;
    }

    @NonNull
    @Override
    public HolderGroupChatList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // Inflate Layouts
        View view = LayoutInflater.from(context).inflate(R.layout.row_group_chatlist, parent,false);

        return new HolderGroupChatList(view);

    }

    @Override
    public void onBindViewHolder(@NonNull HolderGroupChatList holder, int position) {

        // Get Data
        ModelGroupChatsList model = groupChatsLists.get(position);
        String groupId = model.getGroupId();
        String groupIcon = model.getGroupIcon();
        String groupTitle = model.getGroupTitle();

        // Set Data
        holder.groupTittleTv.setText(groupTitle);

        try{
            Picasso.get().load(groupIcon).placeholder(R.drawable.ic_group_primary).into(holder.groupIconIv);
        } catch (Exception e){
            holder.groupIconIv.setImageResource(R.drawable.ic_group_primary);
        }

        // Handle Group Click
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Will Do Later
                Intent intent = new Intent(context, GroupChatActivity.class);
                intent.putExtra("groupId", groupId);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return groupChatsLists.size();
    }

    // View Holder Class
    class HolderGroupChatList extends RecyclerView.ViewHolder{

        // Ui Views
        private ImageView groupIconIv;
        private TextView groupTittleTv, nameTv, messageTv, timeTv;

        public HolderGroupChatList(@NonNull View itemView) {
            super(itemView);

            groupIconIv = itemView.findViewById(R.id.groupIconIv);
            groupTittleTv = itemView.findViewById(R.id.groupTittleTv);
            nameTv = itemView.findViewById(R.id.nameTv);
            messageTv = itemView.findViewById(R.id.messageTv);
            timeTv = itemView.findViewById(R.id.timeTv);
        }
    }
}
