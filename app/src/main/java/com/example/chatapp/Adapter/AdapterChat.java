package com.example.chatapp.Adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.Model.ModelChat;
import com.example.chatapp.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class AdapterChat extends RecyclerView.Adapter<AdapterChat.MyHolder>{

    private static final int MSG_TYPE_LEFT = 0;
    private static final int MSG_TYPE_RIGHT = 1;
    Context context;
    List<ModelChat> chatList;
    String imageUri;
    FirebaseUser fUser;

    public AdapterChat(Context context, List<ModelChat> chatList, String imageUri) {
        this.context = context;
        this.chatList = chatList;
        this.imageUri = imageUri;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        // Inflate layouts : row_chat_left.xml for receiver, row_chat_right.xml for sender
        if(viewType == MSG_TYPE_RIGHT){
            View view = LayoutInflater.from(context).inflate(R.layout.row_chat_right, parent, false);
            return new MyHolder(view);

        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.row_chat_left, parent, false);
            return new MyHolder(view);
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, final int position) {
        // Get data
        String message = chatList.get(position).getMessage();
        String timeStamp = chatList.get(position).getTimestamp();
        String type = chatList.get(position).getType();


        // Convert timestamp to dd/mm/yyyy hh:mm am/pm
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(Long.parseLong(timeStamp));
        String dateTime = DateFormat.format("dd/mm/yyyy hh:mm aa", cal).toString();

        if(type.equals("text")){
            // Text Messages
            holder.Pesan.setVisibility(View.VISIBLE);
            holder.messageIv.setVisibility(View.GONE);
            holder.Pesan.setText(message);
        } else {
            // Image Messages
            holder.Pesan.setVisibility(View.GONE);
            holder.messageIv.setVisibility(View.VISIBLE);
            Picasso.get().load(message).placeholder(R.drawable.ic_image).into(holder.messageIv);
        }

        // Set data
        holder.Pesan.setText(message);
        holder.Time.setText(dateTime);

        try {
            Picasso.get().load(imageUri).into(holder.ProfilePic);
        } catch (Exception e) {

        }
        // Click to Show Delete Dialog
        holder.messageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show Delete Confirm Dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Delete");
                builder.setMessage("Are you sure to delete this message ?");

                // Delete Button
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteMessage(position, v);
                    }
                });
                // Cancel Delete Button
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                // Create and Show Dialog
                builder.create().show();
            }
        });


        // Set seen/delivered status of message
        if (position==chatList.size()-1){

            if (chatList.get(position).isDilihat()){
                holder.Dilihat.setText("Dilihat");
            } else {
                holder.Dilihat.setText("Terkirim");
            }

        } else {
            holder.Dilihat.setVisibility(View.GONE);
        }
    }

    private void deleteMessage(int position, View v) {

        String myUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        // Get Timestamp of clicked message
        // Compare the timestamp of the clicked message with all message in chats
        // Where both values matches delete that messages
        String msgTimeStamp  = chatList.get(position).getTimestamp();
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("Chats");
        Query query  = dbRef.orderByChild("timestamp").equalTo(msgTimeStamp);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot ds :  snapshot.getChildren()){

                    // If they match means its the message of sender that is trying to delete
                    if (ds.child("sender").getValue().equals(myUid)){
                        // Remove the messages from chats
                        //  ds.getRef().removeValue();

                        // Set the value of message "This message was deleted... "
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("message", "This message was deleted...");
                        ds.getRef().updateChildren(hashMap);

                        Snackbar snackbar = Snackbar.make(v, "Message Deleted", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    else {
                         Snackbar snackbar = Snackbar.make(v, "You can delete only your messages...",Snackbar.LENGTH_LONG);
                         snackbar.show();
                    }
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        // Get currently signed in user
        fUser = FirebaseAuth.getInstance().getCurrentUser();
        if(chatList.get(position).getSender().equals(fUser.getUid())){
            return MSG_TYPE_RIGHT;
        } else {
            return  MSG_TYPE_LEFT;
        }

    }

    // Viewholder class

    class MyHolder extends RecyclerView.ViewHolder{

        ImageView ProfilePic, messageIv;
        TextView Pesan, Time, Dilihat;
        RelativeLayout messageLayout; // For Click Listener to Show Delete

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            // Init view
            ProfilePic = itemView.findViewById(R.id.profilePic);
            Pesan = itemView.findViewById(R.id.pesan);
            Time = itemView.findViewById(R.id.time);
            Dilihat = itemView.findViewById(R.id.terkirim);
            messageLayout = itemView.findViewById(R.id.messageLayout);
            messageIv = itemView.findViewById(R.id.messageIv);

        }
    }

}
