package com.example.chatapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.ChatActivity;
import com.example.chatapp.Model.ModelTeman;
import com.example.chatapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterTeman extends RecyclerView.Adapter<AdapterTeman.MyHolder> {

    Context context;
    List<ModelTeman> temanList;

    //constructor
    public AdapterTeman(Context context, List<ModelTeman> temanList) {
        this.context = context;
        this.temanList = temanList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        // inflate layout(row.user.xml)
        View view = LayoutInflater.from(context).inflate(R.layout.row_users, viewGroup, false );
        return new MyHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        // get Data
        String hisUID   = temanList.get(position).getUid();
        String userImage = temanList.get(position).getImage();
        String userName = temanList.get(position).getName();
        String userEmail = temanList.get(position).getEmail();

        // set Data
        holder.namaTeman.setText(userName);
        holder.emailTeman.setText(userEmail);
        try{
            Picasso.get().load(userImage)
                    .placeholder(R.drawable.ic_default_image)
                    .into(holder.avatar);
        }catch (Exception e){

        }
        // handle item
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("hisUid", hisUID);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return temanList.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{
        ImageView avatar;
        TextView namaTeman, emailTeman;
        public MyHolder(@NonNull View itemView){
            super(itemView);

            // Init views
            avatar = itemView.findViewById(R.id.avatarteman);
            namaTeman = itemView.findViewById(R.id.namateman);
            emailTeman = itemView.findViewById(R.id.emailteman);

        }
    }
}
