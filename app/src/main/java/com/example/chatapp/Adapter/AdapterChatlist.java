package com.example.chatapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatapp.ChatActivity;
import com.example.chatapp.Model.ModelChatlist;
import com.example.chatapp.Model.ModelTeman;
import com.example.chatapp.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

public class AdapterChatlist extends RecyclerView.Adapter<AdapterChatlist.MyHolder>{

    Context context;
    List<ModelTeman> TemanList; // Get user Info
    List<ModelChatlist> chatlistList;
    private final HashMap<String, String> lastMessageMap;


    // Constructor
    public AdapterChatlist(Context context, List<ModelTeman> TemanList) {
        this.context = context;
        this.TemanList = TemanList;
        lastMessageMap = new HashMap<>();
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate Layout
        View view = LayoutInflater.from(context).inflate(R.layout.row_chatlist, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        // Get Data
        String hisUid  = TemanList.get(position).getUid();
        String userImage  = TemanList.get(position).getImage();
        String userName  = TemanList.get(position).getName();
        String lastMessage = lastMessageMap.get(hisUid);

        // Set Data
        holder.nameTv.setText(userName);
        if (lastMessage!=null || lastMessage.equals("default")){
            holder.lastMessage.setVisibility(View.GONE);
        } else {
            holder.lastMessage.setVisibility(View.VISIBLE);
            holder.lastMessage.setText(lastMessage);
        }
        // Panggil Gambar
        try {
            Picasso.get().load(userImage).placeholder(R.drawable.ic_default_image).into(holder.profileIv);

        } catch (Exception e){
            Picasso.get().load(R.drawable.ic_default_image).into(holder.profileIv);
        }
        // Set online status of other users in chatlist
        if (TemanList.get(position).getOnlineStatus().equals("online")){
            // Online
            holder.onlineStatusIv.setImageResource(R.drawable.circle_online);

        } else {
            // Ofline
            holder.onlineStatusIv.setImageResource(R.drawable.circle_offline);
        }

        // Handle Click of User in chatlist
        holder.itemView.setOnClickListener(v -> {
            // Start chat activity with that user
            Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra("hisUid", hisUid);
            context.startActivity(intent);
        });

    }

    public void setLastMessageMap(String userId, String lastMessage){
        lastMessageMap.put(userId, lastMessage);
    }

    @Override
    public int getItemCount() {
        return TemanList.size(); // Size of List
    }


    static class MyHolder extends RecyclerView.ViewHolder{
        // Views of row_chatlist
        ImageView profileIv, onlineStatusIv;
        TextView nameTv, lastMessage;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            profileIv = itemView.findViewById(R.id.profileIv);
            onlineStatusIv = itemView.findViewById(R.id.onlineStatusIv);
            nameTv = itemView.findViewById(R.id.nameTv);
            lastMessage = itemView.findViewById(R.id.lastMessageTv);


        }
    }
}
