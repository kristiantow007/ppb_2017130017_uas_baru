package com.example.chatapp.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.example.chatapp.Adapter.AdapterTeman;
import com.example.chatapp.CreateGroupActivity;
import com.example.chatapp.LoginActivity;
import com.example.chatapp.Model.ModelTeman;
import com.example.chatapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TemanFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TemanFragment extends Fragment {

    RecyclerView recyclerView;
    AdapterTeman adapterTeman;
    List<ModelTeman> temanList;
    private FirebaseAuth firebaseAuth;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public TemanFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TemanFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TemanFragment newInstance(String param1, String param2) {
        TemanFragment fragment = new TemanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    //  Default start set init
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teman, container, false);

        // init Firebase
        firebaseAuth = FirebaseAuth.getInstance();

        // Init Recyler
        recyclerView = view.findViewById(R.id.teman_recylerView);

        // Set its properties
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Init teman list
        temanList = new ArrayList<>();
        // get teman
        getAllTeman();

        return view;
    }

    // Ambil data teman dari database

    private void getAllTeman(){
        // get current user
        FirebaseUser fUSer = FirebaseAuth.getInstance().getCurrentUser();
        // get path database
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                temanList.clear();
                for (DataSnapshot ds: snapshot.getChildren()){
                    ModelTeman modelTeman = ds.getValue(ModelTeman.class);

                    // get all user
                    if(!modelTeman.getUid().equals(fUSer.getUid())){
                        temanList.add(modelTeman);
                    }
                    adapterTeman = new AdapterTeman(getActivity(),temanList);

                    // set adapter to recyler view
                    recyclerView.setAdapter(adapterTeman);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    // Mulai mencari teman
    private void searchTeman(String query) {
        // get current user
        FirebaseUser fUSer = FirebaseAuth.getInstance().getCurrentUser();
        // get path database
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                temanList.clear();
                for (DataSnapshot ds: snapshot.getChildren()){
                    ModelTeman modelTeman = ds.getValue(ModelTeman.class);

                    // get all searched users except currently signed in user
                    // Kondisi 1. bukan user, 2.username dan email case sensitive

                    if(!modelTeman.getUid().equals(fUSer.getUid())){

                        if(modelTeman.getName().toLowerCase().contains(query.toLowerCase()) ||
                            modelTeman.getEmail().toLowerCase().contains(query.toLowerCase())) {
                            temanList.add(modelTeman);
                        }
                    }

                    // Adapter
                    adapterTeman = new AdapterTeman(getActivity(),temanList);
                    // refresh adapter
                    adapterTeman.notifyDataSetChanged();
                    // set adapter to recyler view
                    recyclerView.setAdapter(adapterTeman);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    // Check Status Online/Tidak
    private void checkUserStatus(){
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if(user !=null){

        }else{
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        }

    }

    // Panggil item Menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main,menu);
        // Search view
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        // Search Listener // Panggil method cari teman
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String q) {
                // called when use button from keyboard / key pressed
                // if search query is not empty then search
                if(!TextUtils.isEmpty(q.trim())){
                // panggil methode cari teman
                    searchTeman(q);

                } else {
                    // jika textbox kosong teman tidak dipanggil
                    getAllTeman();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String q) {
                // called when use button from keyboard / key pressed
                // if search query is not empty then search
                if(!TextUtils.isEmpty(q.trim())){
                    // search text contains text, search it
                    searchTeman(q);

                } else {
                    // Search text empty, get all users
                    getAllTeman();
                }
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }


    // Show menu
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true); // to show menu item in fragment

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    // Menu logout
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_logout){
            firebaseAuth.signOut();
            checkUserStatus();
        } // Go to group activity
        else if ( id==R.id.action_create_group) {
            startActivity(new Intent(getActivity(), CreateGroupActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }


}

//Model class for recyler view