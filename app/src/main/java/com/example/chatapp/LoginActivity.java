package com.example.chatapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public class LoginActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 100;
    GoogleSignInClient mGoogleSignInClient;

    TextInputEditText Etemail, EtPassword;
    Button Login;
    TextView Register, LupaPassword;
    ImageView GoogleLogin;

    ProgressDialog progressDialog;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Login       = findViewById(R.id.BtnLogin);
        Register    = findViewById(R.id.BtnRegister);
        Etemail     = findViewById(R.id.txtEmail);
        EtPassword    = findViewById(R.id.txtPassword);
        GoogleLogin   = findViewById(R.id.googleLogin);
        LupaPassword = findViewById(R.id.txtLupaPassword);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);

        mAuth = FirebaseAuth.getInstance();

        // Init Progress Dialog
        progressDialog = new ProgressDialog(this);

        // Handle Login Click
        Login.setOnClickListener(v -> {
            String email = Objects.requireNonNull(Etemail.getText()).toString();
            String password = Objects.requireNonNull(EtPassword.getText()).toString().trim();

            if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Etemail.setError("Invalid Email");
                Etemail.setFocusable(true);
            } else if (password.length() < 6) {
                EtPassword.setError("Password length at least 6 characters");
                EtPassword.setFocusable(true);
            } else {
                loginUser(email, password, v);
            }
        });

        // Handle Register Click
        Register.setOnClickListener(v -> {
           startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
           finish();
        });

        // Handle Google Login Click
        GoogleLogin.setOnClickListener(v -> {
            // StarT Login
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        });

        // Handle Recover Password CLick
        LupaPassword.setOnClickListener(v ->
                showRecoverPasswordDialog());

    }

    private void showRecoverPasswordDialog() {

        // Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Recover Password");

        // Set layout linear layout
        LinearLayout linearLayout = new LinearLayout(this);
        // Views to set in dialog
        EditText txtEmail = new EditText(this);
        txtEmail.setHint("Email");
        txtEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        // Set the min of width of a editView to fit a text
        txtEmail.setMinEms(16);

        linearLayout.addView(txtEmail);
        linearLayout.setPadding(10,10,10,10);
        builder.setView(linearLayout);

        // Buttons Recover
        builder.setPositiveButton("Recover", (dialog, which) -> {
            // Input Email
            String email = txtEmail.getText().toString().trim();
            beginRecoveryEmail(email);
        });

        // Buttons Cancel
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());
        builder.create().show();

    }

    private void beginRecoveryEmail(String email) {
        // Show Progress Dialog
        progressDialog.setMessage("Sending email...");
        progressDialog.show();
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(task -> {

                    if(task.isSuccessful()){
                        Toast.makeText(LoginActivity.this, "Email Sent... !", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, "Failed...", Toast.LENGTH_SHORT).show();
                    }

                    // Close Progress Dialog
                    progressDialog.dismiss();
                }).addOnFailureListener(e -> {
                    Toast.makeText(LoginActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                     // Close Progress Dialog
                    progressDialog.dismiss();

        });
    }

    private void loginUser (String email, String password, View v){
        progressDialog.setMessage("Logging In...");
        progressDialog.show();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        progressDialog.dismiss();
                        mAuth.getCurrentUser();

                        startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                        finish();
                    } else {
                        // If sign in fails, display a message to the user.
                        progressDialog.dismiss();

                    }

                }).addOnFailureListener(e -> {
                    progressDialog.dismiss();
                    Snackbar snackbar = Snackbar.make(v, "Authentication Failed, " + e.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();

                });
    }

    // Ovveride Firebase With Google
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                assert account != null;
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(LoginActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                // ...
            }
        }
    }
    // Method Login with Firebase Google
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct){

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()){
                        // Sign-in succes update Ui with sign-in google
                        FirebaseUser user = mAuth.getCurrentUser();

                        if(Objects.requireNonNull(Objects.requireNonNull(task.getResult()).getAdditionalUserInfo()).isNewUser()){
                            // Get Email and Uid
                            assert user != null;
                            String email = user.getEmail();
                            String uid = user.getUid();

                            // Ketika berhasil registrasi simpan data ke firebase
                            // Menggunakan hashmap
                            AtomicReference<HashMap<Object, String>> hashMap = new AtomicReference<>(new HashMap<>());
                            //Putinfo
                            hashMap.get().put("email", email);
                            hashMap.get().put("uid", uid);
                            hashMap.get().put("name", ""); // Add Later Edit Profile
                            hashMap.get().put("onlineStatus", "online"); // Add Later Edit Profile
                            hashMap.get().put("phone", ""); // Add Later Edit Profile
                            hashMap.get().put("image", ""); // Add Later Edit Profile
                            hashMap.get().put("cover", ""); // Add Later Edit Profile
                            // Firebase database instance
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            // Path to store user data named "Users"
                            database.getReference("Users");

                            // User email in toast
                            Toast.makeText(LoginActivity.this, ""+user.getEmail(), Toast.LENGTH_SHORT).show();
                            // Go to profile after looged in
                            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                            finish();

                        } else {
                            Toast.makeText(LoginActivity.this, "Login Failed...", Toast.LENGTH_SHORT).show();
                        }

                    }
                }).addOnFailureListener(e -> {
                    // Show Error Message
                    Toast.makeText(LoginActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                });

    }

}


