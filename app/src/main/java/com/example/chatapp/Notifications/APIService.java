package com.example.chatapp.Notifications;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key= AAAAVNbflyE:APA91bGkTNSLidNu4VdZTjkUvDMuXeDyBAqotUa-ZL5oa-28bL6lpZ324cWaTMnSIZpWz--9rT4XQWdNF3WMBo8wJvB2wvt51YhWZdkOa_n4HWI2DfpYpDqFAH577onvFYNn_uksYXzz"

    })
    @POST("fcm/send")
    Call<Response> sendNotification(@Body Sender body);
}
